from django.shortcuts import redirect, render
from regist.models import student


def home_page(request):
    if request.method == 'POST':
        student.objects.create(name=request.POST['name_id'] ,
                               surname=request.POST['surname_id'],
                               nickname =request.POST['nickname_id'], 
                               address =request.POST['address_id'],
                               telephone =request.POST['telephone_id'],
                               school =request.POST['school_id'],
                               age =request.POST['age_id'],)

        return redirect('/success/')
    return render(request, 'home.html')

def success_page(request):
    if request.method == 'POST':
        return redirect('/')
    return render(request, 'success.html')
