# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='student',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.TextField(default='')),
                ('surname', models.TextField(default='')),
                ('nickname', models.TextField(default='')),
                ('address', models.TextField(default='')),
                ('telephone', models.TextField(default='')),
                ('school', models.TextField(default='')),
                ('age', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
