from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
import pep8
from regist.models import student
from regist.views import home_page


class HomeTest(TestCase):
    
    def test_send_data_to_db(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['name_id'] = 'panlapa'
        request.POST['surname_id'] = 'khemarungsarit'
        request.POST['nickname_id'] = 'preaw'
        request.POST['address_id'] = 'bangkok'
        request.POST['telephone_id'] = '0819170757'
        request.POST['school_id'] = 'kmutnb'
        request.POST['age_id'] = '20'
        response = home_page(request)
        s = student.objects.first()
        self.assertEqual(s.name, 'panlapa')
        self.assertEqual(s.surname, 'khemarungsarit')
        self.assertEqual(s.nickname, 'preaw')
        self.assertEqual(s.address, 'bangkok')
        self.assertEqual(s.telephone, '0819170757')
        self.assertEqual(s.school, 'kmutnb')
        self.assertEqual(s.age, '20')

    def test_go_to_success(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['name_id'] = 'panlapa'
        request.POST['surname_id'] = 'khemarungsarit'
        request.POST['nickname_id'] = 'preaw'
        request.POST['address_id'] = 'bangkok'
        request.POST['telephone_id'] = '0819170757'
        request.POST['school_id'] = 'kmutnb'
        request.POST['age_id'] = '20'
        response = home_page(request)
        self.assertEqual(response['location'], '/success/')
