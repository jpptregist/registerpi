from django.db import models

class student(models.Model):
    name = models.TextField(default='')
    surname = models.TextField(default='')
    nickname = models.TextField(default='')
    address = models.TextField(default='')
    telephone = models.TextField(default='')
    school = models.TextField(default='')
    age = models.TextField(default='')
