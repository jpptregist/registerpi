# Overview #

เก็บข้อมูลการลงทะเบียนนักเรียนที่สนใจศึกษาราสเบอรี่พาย

## Features ##

* เก็บข้อมูลชื่อ อายุ ที่อยู่ เบอร์โทรและโรงเรียน

## Use cases ##

* เก็บข้อมูล

1. กรอกชื่อในช่องสำหรับกรอกรายชื่อ
2. กรอกนามสกุลในช่องสำหรับกรอกนามสกุล
3. กรอกชื่อเล่นในช่องสำหรับกรอกชื่อเล่น
4. กรอกอายุในช่องสำหรับกรอกอายุ
5. กรอกที่อยู่ในช่องสำหรับกรอกที่อยู่
6. กรอกเบอร์โทรในช่องสำหรับกรอกเบอร์โทร
7. กรอกโรงเรียนในช่องสำหรับกรอกโรงเรียน
8. กด submit